# Dictionary
variable "workspace_dictionary" {
  type = object({
    default = string
    stage = string
    prod = string
  })
  default = ({
    default = "devel"
    stage = "stage"
    prod = "prod"
  })
}

# Bucket configs
variable "bucket_name" {
  type = string
  default = "fsl"
}

variable "aws_region" {
  type = string
  default = "us-east-1"
}

# Cloudflare DNS/Zone
variable "domain" {
  type = string
  default = "mancier.me"
}

variable "zone_id" {
  type = string
  default = "38b57da770c9105adc0e9a9ff4a6bb27"
}

# Credentials
variable "aws_access_key" {}
variable "aws_secret_access_key" {}
variable "cloudflare_api_token" {}