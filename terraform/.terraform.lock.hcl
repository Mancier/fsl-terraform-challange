# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.25.0"
  constraints = "~> 3.0"
  hashes = [
    "h1:0c3vT05Tnid8JMHreNR2iglNg7eelMYHDFd+GyFOgxY=",
    "zh:0686829813196c88aa99aad24f13e4a5fefed551e10ebeb27c7f4745b3f9746c",
    "zh:13f57e4e89483790b8c4f4040767cfe9b6da5d0f3c7d444e4986c89cd65ac393",
    "zh:2ce13f29c27ed8353f6145e8a4ead571992a35b08127be6fe27a54ee73e096a8",
    "zh:57c580e7d394ad109bcb88e2aaf1f67ae3819e4139bc694baa52c14ce7cbe707",
    "zh:6ff57e101b9157109e4b37decdb75b653d5cd1fbb06e369270d43da0b99bcd48",
    "zh:947b6b53863956ba0a1f3ea43972603071e61a0ed247f9635c8c0a2414d82b8c",
    "zh:9f2fac64de5481523c7d1e58ecbf1b912bfbe5c4dc97b51fa393eede12257db7",
    "zh:a964116518588c80bf9bcafe24ecdf5d2513fe485a8b04e0a514c4343a906e41",
    "zh:c1f0efd66dc246946cf8c0219de4f23a3c77f740550b723522e1e228fce09097",
    "zh:ce0781c9ab2a9a636796a2eea765d38f82cb7bacd8cd57d92c72e7b9fe53c6f1",
    "zh:ceb6de28c6daa396e327861db51082ec08c1188213f714706449e40c9cde1e74",
    "zh:e856cb211bc4bc8632723eaab1398f99a4f2e57a5c7ebe092cf6bedb49ae01fc",
    "zh:f53bf6525b9e178691e337c383615e59f231e6b3b77e72266eeee22b3c7dac2b",
    "zh:f70f1af2890c8cf193603c0537d51feaabaad516b9431d204ace51f80bfd5a37",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "4.33.0"
  constraints = "~> 4.0"
  hashes = [
    "h1:2MWU+HIKKivfhY8dAU1cR0xxwlzNrWOZEQs8BApQ/Ao=",
    "zh:421b24e21d7fac4d65d97438d2c0a4effe71d3a1bd15820d6fde2879e49fe817",
    "zh:4378a84ca8e2a6990f47abc24367b801e884be928671b37ad7b8e7b656f73e48",
    "zh:54e0d7884edf3cefd096715794d32b6532138dca905f0b2fe84fb2117594293c",
    "zh:6269a7d0312057db5ded669e9f7f9bd80fb6dcb549b50d8d7f3f3b2a0361b8a5",
    "zh:67f57d16aa3db493a3174c3c5f30385c7af9767c4e3cdca14e5a4bf384ff59d9",
    "zh:7d4d4a1d963e431ffdc3348e3a578d3ba0fa782b1f4bf55fd5c0e527d24fed81",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:cd8e3d32485acb49c1b06f63916fec8e73a4caa6cf88ae9c4bf236d6f5d9b914",
    "zh:d586fd01195bd3775346495e61806e79b6012e745dc05e31a30b958acf968abe",
    "zh:d76122060f25ab87887a743096a42d47ba091c2c019ac13ce6b3973b2babe5a3",
    "zh:e917d36fe18eddc42ec743b3152b4dcb4853b75ea7a679abd19bdf271bc48221",
    "zh:eb780860d5c04f43a018aef564e76a2d84e9aa68984fa1f968ca8c09d23a611a",
  ]
}
