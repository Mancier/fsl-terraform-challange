resource "cloudflare_record" "dns" {
  name    = aws_s3_bucket.bucket.bucket
  value = aws_s3_bucket_website_configuration.website.website_endpoint
  type    = "CNAME"
  proxied = true
  ttl = 1
  zone_id = var.zone_id
}