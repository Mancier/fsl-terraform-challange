# S3 bucket
# s3 website configuration
# s3 policy
# s3 acl

# fsl-devel.mancier.me
resource "aws_s3_bucket" "bucket" {
  bucket = "${var.bucket_name}-${var.workspace_dictionary[terraform.workspace]}.${var.domain}"
}

resource "aws_s3_bucket_website_configuration" "website" {
  bucket = aws_s3_bucket.bucket.id
  index_document {
    suffix = "index.html"
  }
}

resource "aws_s3_bucket_acl" "acl" {
  bucket = aws_s3_bucket.bucket.id
  acl = "public-read"
}

resource "aws_s3_bucket_policy" "policy" {
  bucket = aws_s3_bucket.bucket.id
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "PublicReadGetObject",
        "Effect": "Allow",
        "Principal": "*",
        "Action": [
          "s3:GetObject"
        ],
        "Resource": [
          aws_s3_bucket.bucket.arn,
          "${aws_s3_bucket.bucket.arn}/*"
        ]
      }
    ]
  })
}

resource "aws_s3_object" "content" {
  for_each = module.dir.files
  bucket = aws_s3_bucket.bucket.id
  source = each.value.source_path
  content = each.value.content
  content_type = each.value.content_type
  key = each.key
  etag = each.value.digests.md5
}